## Table of Contents

- [Prerequisites](#prerequisites)
- [Getting Started](#getting-started)
- [Folder Structure](#folder-structure)
- [Description](#description)

## Prerequisites

Make sure you have npm insalled.

## Getting Started

In the project directory, run:

### `npm install`

Install dependencies and set the project up.

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

You must wait until the data from API is collected for the UI to be populated with data.

### `npm test`

Launches the test runner in the interactive watch mode.

## Folder Structure

```
progress-bar-demo/
  README.md
  node_modules/
  package.json
  public/
    index.html
    favicon.ico
  src/
    components/
      App.js
      App.test.js
      ProgressBar.js
      ProgressBar.test.js
      setupTests.js
    index.scss
    index.js
    Services.js
```

## Description

The React app has been created using create-react-app and no JQuery has been used anywhere. Resoponsiveness has been added using Bootstrap
Reusable react components have been used.
ESLint has been utilized for code analysis.
Minification is automatically managed by webpack from create-react-app.