import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

it('renders without crashing', () => { 
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
});

test('calculateDisplayPercentage', () => {
  expect(App.calculateDisplayPercentage(1, 2)).toBe(50);
  expect(App.calculateDisplayPercentage(-2, 5)).toBe(0);
  expect(App.calculateDisplayPercentage(10, 5)).toBe(200);
  // expect(App.calculateDisplayPercentage(10, -5)).toThrow();
});
