import React from 'react';
import ProgressBar from './ProgressBar';

import { configure } from 'enzyme';
import { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

it('renders progress bar', () => {
  const progressBar = shallow(<ProgressBar percentage="20"/>);
  expect(progressBar.find('div').length).toBe(4);
  expect(progressBar.find('.progress-lbl').text()).toBe('20%');
  expect(progressBar.find('.filler').props().style.width).toBe('20%');
  
})

it('renders progress bar', () => {
  const progressBar = shallow(<ProgressBar percentage="0"/>);
  expect(progressBar.find('div').length).toBe(4);
  expect(progressBar.find('.progress-lbl').text()).toBe('0%');
  expect(progressBar.find('.filler').props().style.width).toBe('0%');
})
