import React from 'react';

var ProgressBar = function(props) {
    return (
      <div>
        <div className="progress-bar">
          <div className="progress-lbl">
            {props.percentage}
            %
          </div>
          <div
            className={props.percentage <= 100 ? 'filler' : 'filler-over'}
            style={{ width: `${props.percentage >= 100 ? 100 : props.percentage}%` }}
          />
        </div>
      </div>
    );
  }


export default ProgressBar;
