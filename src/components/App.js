import React from 'react';
import ProgressBar from './ProgressBar';
import Services from '../Services';

class App extends React.Component {
  static calculateDisplayPercentage(progress, limit) {
    if (limit < 0) {
      throw new Error('invalid parameters!');
    }
    const val = Math.round(progress / limit * 100);
    return val > 0 ? val : 0;
  }

  constructor(props) {
    super(props);

    this.state = {
      progress: [0],
      selected: 0,
      buttonVals: [0],
      limit: 0,
    };
  }

  componentDidMount() {
    Services.getProgressBarDetails()
      .then((result) => {
        console.log(result);
        const progressBars = result.bars;
        const buttonVals = result.buttons;
        const { limit } = result;

        this.setState(
          {
            progress: progressBars,
            selected: 0,
            buttonVals,
            limit,
          },
        );
      })
      .catch((error) => {
        console.log(error);
      });
  }

  handleClick(val) {
    const progress = this.state.progress.slice();
    progress[this.state.selected] = progress[this.state.selected] + val.name;
    this.setState({
      progress,
      selected: this.state.selected,
      buttonVals: this.state.buttonVals,
      limit: this.state.limit,
    });
  }

  handleChange(event) {
    console.log(event.target.selectedIndex);
    this.setState({
      progress: this.state.progress,
      selected: event.target.selectedIndex,
      buttonVals: this.state.buttonVals,
      limit: this.state.limit,
    });
  }

  renderProgressBar(index, progress) {
    return (
      <ProgressBar key={index} percentage={App.calculateDisplayPercentage(progress, this.state.limit)} />
    );
  }

  renderProgressBars() {
    const { progress } = this.state;
    return progress.map((name, index) => this.renderProgressBar(index, progress[index]));
  }

  render() {
    return (
      <div>
        <div className="container panel">

          <div className="row header-box">
            <h1 className="header-line">Progress Bars Demo</h1>
          </div>
          <div className="row data-box">

            <div className="col-md-12 col-lg-4 col-sm-12 bars-panel">
              {this.renderProgressBars()}
            </div>

            <div className="col-md-4 col-lg-4 col-sm-12 progress-combo-panel">
              <select className="select-bar" defaultValue={this.state.selected} onChange={e => this.handleChange(e)}>
                {this.state.progress.map((name, index) => (
                  <option key={index}>
                    #progress
                    {index + 1}
                  </option>
                ))}
              </select>
            </div>

            <div className="col-md-8 col-lg-4 col-sm-12">
              <div className="button-panel">
                {this.state.buttonVals.map(function (name, index) {
                  return <button key={index} className="value_button" onClick={() => this.handleClick({ name })}>{name}</button>;
                }, this)}
              </div>
            </div>

          </div>
        </div>
      </div>
    );
  }
}

export default App;
