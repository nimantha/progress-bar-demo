class Services {
    static PROGRESS_BAR_ENDPOINT = 'http://pb-api.herokuapp.com/bars/api';

  static getProgressBarDetails() {
    const promise = fetch(Services.PROGRESS_BAR_ENDPOINT)
      .then(response => response.json());

    promise.catch((error) => {
      alert("Failed to collect data from api! " + error);
    });
    return promise;
  }
}

export default Services;
